package com.codeoftheweb.salvo.controller;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.hibernate.annotations.common.reflection.XMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api")
public class SalvoController {


    //RELATIONS//
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private GameRepository gameRepository;
    @Autowired
    private ShipRepository shipRepository;
    @Autowired
    private GamePlayerRepository gamePlayerRepository;
    @Autowired
    private SalvoRepository salvoRepository;

    public SalvoController() {
    }

    private Map<String, Object> makeMap(String key, Object value) {
        Map<String, Object> map = new HashMap<>();
        map.put(key, value);
        return map;
    }

    //METHODS//
    @RequestMapping("/games")
    public Map<String, Object> getAll(Authentication authentication) {
        Map<String, Object> dto = new LinkedHashMap<>();
        if (isGuest(authentication)) {
            dto.put("player", "Guest");
        } else {
            Player player = playerRepository.findByUserName(authentication.getName());
            dto.put("player", player.makeListDTO());
        }
        dto.put("games", gameRepository.findAll()
                .stream()
                .map(games -> games.gamesDTO())
                .collect(Collectors.toList()));
        return dto;
    }

    @RequestMapping(path = "/games", method = RequestMethod.POST)
    public ResponseEntity<Object> createGame(Authentication authentication) {

        if (isGuest(authentication)) {
            return new ResponseEntity<>("No esta autorizado", HttpStatus.UNAUTHORIZED);
        }
        Player player = playerRepository.findByUserName(authentication.getName());
        if (player == null) {
            return new ResponseEntity<>("No hay jugadores", HttpStatus.FORBIDDEN);
        }
        Game game = gameRepository.save(new Game(LocalDateTime.now()));
        GamePlayer gamePlayer = gamePlayerRepository.save(new GamePlayer(game, player));
        return new ResponseEntity<>(makeMap("gpid", gamePlayer.getId()), HttpStatus.CREATED);


    }


    @RequestMapping("/gamesList")
    public List<Long> getId() {
        return gameRepository
                .findAll()
                .stream()
                .map(game -> game.getId())
                .collect(Collectors.toList());
    }


    //mapping para hacer el path de una url//
    @RequestMapping("game_view/{nn}")
    private ResponseEntity<Map<String, Object>> getGameViewByGamePlayerId(@PathVariable Long nn, Authentication authentication) {


        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "missing data"), HttpStatus.UNAUTHORIZED);
        }

        GamePlayer gamePlayer = gamePlayerRepository.findById(nn).get();
        Player player = playerRepository.findByUserName(authentication.getName());

        if (player == null) {

            return new ResponseEntity<>(makeMap("error", "something happen"), HttpStatus.UNAUTHORIZED);
        }

        if (gamePlayer == null) {

            return new ResponseEntity<>(makeMap("error", "something happen"), HttpStatus.UNAUTHORIZED);
        }

        if (gamePlayer.getPlayer().getId() != player.getId()) {

            return new ResponseEntity<>(makeMap("error", "something happen"), HttpStatus.FORBIDDEN);
        }
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", gamePlayer.getGame().getId());
        dto.put("created", gamePlayer.getGame().getCreationDate());
        dto.put("gameState", "PLACESHIPS");
        dto.put("gamePlayers", gamePlayer.getGame().getGamePlayer()
                .stream()
                .map(gamePlayer2 -> gamePlayer2.gamePlayerDTO())
                .collect(Collectors.toList()));
        dto.put("ships", gamePlayer.getShip().stream().map(type -> type.makeDTO())
                .collect(Collectors.toList()));
        dto.put("salvoes", gamePlayer.getGame().getGamePlayer().stream()
                .map(gamePlayer1 -> gamePlayer1.getListaSalvo())
                .flatMap(salvos -> salvos.stream()
                        .map(salvo -> salvo.salvosDTO())));
        dto.put("hits", hitsDTO());


        return new ResponseEntity<>(dto, HttpStatus.OK);


    }

    private Map<String, ArrayList> hitsDTO() {
        Map<String, ArrayList> dto = new LinkedHashMap<>();
        dto.put("self", new ArrayList<String>());
        dto.put("opponent", new ArrayList<String>());
        return dto;
    }


    // metodo para poder registrar usuarios
    @RequestMapping(path = "/players", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> register(

            @RequestParam String email, @RequestParam String password) {


        if (email.isEmpty() || password.isEmpty()) {
            return new ResponseEntity<>(makeMap("error", "Missing data"), HttpStatus.FORBIDDEN);
        }

        if (playerRepository.findByUserName(email) != null) {
            return new ResponseEntity<>(makeMap("error", "Name already in use"), HttpStatus.FORBIDDEN);
        }

//        if (playerRepository.findByUserName(email) ==  playerRepository.findByUserName(email)) {
//            return new ResponseEntity<>("Name already in use", HttpStatus.FORBIDDEN);
//        }

        playerRepository.save(new Player(email, passwordEncoder.encode(password)));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    private boolean isGuest(Authentication authentication) {
        return authentication == null || authentication instanceof AnonymousAuthenticationToken;
    }


    @PostMapping("game/{nn}/players")
    private ResponseEntity<Map<String, Object>> joinGame(@PathVariable Long nn, Authentication authentication) {


        if (isGuest(authentication)) {
            return new ResponseEntity<>(makeMap("error", "missing data"), HttpStatus.UNAUTHORIZED);
        }

        Player player = playerRepository.findByUserName(authentication.getName());
        Game game = gameRepository.getOne(nn);

        if (game == null) {

            return new ResponseEntity<>(makeMap("error", "no such game"), HttpStatus.FORBIDDEN);

        }
        if (game.getGamePlayer().size() <= 2) {

            GamePlayer gamePlayer = gamePlayerRepository.save(new GamePlayer(game, player));
            return new ResponseEntity<>(makeMap("gpid", gamePlayer.getId()), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(makeMap("error", "no capo esta lleno"), HttpStatus.FORBIDDEN);
        }
    }

//    @PutMapping("/player/{playerId}/edit")
//    public ResponseEntity<Map<String,Object>> editPlayer(@PathVariable Player player, Authentication authentication){
//
//        Player player = playerRepository.findByUserName(authentication.getName());
//
//        //vacio, que no este repetido
//
//        if (player == player){
//            ResponseEntity<>(makeMap("eu amigo","cambiate el nick gil"),.HttpStatus.FORBIDDEN);
//        }


    @RequestMapping(value = "/games/players/{gamePlayerId}/ships", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> placeShips(@PathVariable Long gamePlayerId, @RequestBody List<Ship> ships, Authentication authentication) {
        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).get();
        Player player = playerRepository.findByUserName(authentication.getName());


        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "you aren't login"), HttpStatus.UNAUTHORIZED);
        }
        if (player == null) {
            return new ResponseEntity<>(makeMap("error", "Payer's null"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.getId() != gamePlayerId) {
            return new ResponseEntity<>(makeMap("error", "that's not your id"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.getShip().size() > 0) {
            return new ResponseEntity<>(makeMap("error", "ship are already placed"), HttpStatus.FORBIDDEN);
        }
        ships.forEach(ship -> ship.setGamePlayer(gamePlayer));
        ships.forEach(ship -> shipRepository.save(ship));
        return new ResponseEntity<>(makeMap("OK", "BIEN CRACK COLOCASTE LOS BARQUITOS"), HttpStatus.CREATED);

    }

    @RequestMapping(value = "/games/players/{gamePlayerId}/salvoes", method = RequestMethod.POST)
    public ResponseEntity<Map<String, Object>> shotingSalvos(@PathVariable Long gamePlayerId, @RequestBody Salvo salvo, Authentication authentication) {
        GamePlayer gamePlayer = gamePlayerRepository.findById(gamePlayerId).get();
        Player player = playerRepository.findByUserName(authentication.getName());

        if (gamePlayer == null) {
            return new ResponseEntity<>(makeMap("error", "you aren't login"), HttpStatus.UNAUTHORIZED);
        }
        if (player == null) {
            return new ResponseEntity<>(makeMap("error", "username empty"), HttpStatus.UNAUTHORIZED);
        }
        if (gamePlayer.getId() != gamePlayerId) {
            return new ResponseEntity<>(makeMap("error", "wrong Id"), HttpStatus.UNAUTHORIZED);

        }

        if (gamePlayer.getListaSalvo().stream().anyMatch(salvo1 -> salvo1.getTurn()==salvo.getTurn())) {
//            System.out.println("********************************hola");
//            System.out.println(salvo.getLocations());
            return new ResponseEntity<>(makeMap("error", "you have already shot"), HttpStatus.FORBIDDEN);
        }else {
            salvo.setGamePlayer(gamePlayer);
            salvoRepository.save(salvo);
            return new ResponseEntity<>(makeMap("OK", "capo"), HttpStatus.CREATED);

        }
    }

//    private getFirst(int turn){
//        for (){}
//    }
}








