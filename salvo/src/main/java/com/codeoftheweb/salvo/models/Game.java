package com.codeoftheweb.salvo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Game {

    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER)
    Set<Score> scores;
    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER)
    Set<Salvo> salvoGame;
    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER)
    Set<GamePlayer> gamePlayers;
    @OneToMany(mappedBy = "game", fetch = FetchType.EAGER)
    Set<Player> players;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private Long id;
    private LocalDateTime creationDate;
    public Game() {
    }
    public Game(LocalDateTime localDateTime) {
        this.creationDate = localDateTime;
    }


    //GETTERS & SETTERS//

    public LocalDateTime getCreationDate() {
        return creationDate;

    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Salvo> getSalvoGame() {
        return salvoGame;
    }

    public void setSalvoGame(Set<Salvo> salvoGame) {
        this.salvoGame = salvoGame;
    }

    public Set<Score> getScores() {
        return scores;
    }

    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }

    public Set<GamePlayer> getGamePlayer() {
        return gamePlayers;
    }

    public void setGamePlayers(Set<GamePlayer> gamePlayers) {
        this.gamePlayers = gamePlayers;
    }

//    public Player getPlayers() {
//        return players;
//    }
//
//    public void setPlayers(Player players) {
//        this.players = players;
//    }


    //    @Autowired
//    private Player players;
    public Map<String, Object> gamesDTO() {
        Map<String, Object> dto = new LinkedHashMap<String, Object>();
        dto.put("id", this.getId());
        dto.put("created", this.getCreationDate());
        dto.put("gamePlayers", this.getGamePlayer().stream().map(gamePlayer -> gamePlayer.gamePlayerDTO()).collect(Collectors.toList()));
        dto.put("scores", this.getScores()
                .stream()
                .map(score -> score.scoreDTO())
                .collect(Collectors.toList()));
        return dto;
    }


}



