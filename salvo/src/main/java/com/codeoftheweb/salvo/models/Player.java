package com.codeoftheweb.salvo.models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

@Entity
public class Player {


    //RELATIONS//
    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    Set<Score> scores;
    @OneToMany(mappedBy = "player", fetch = FetchType.EAGER)
    Set<GamePlayer> gameGamePlayer;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    private long id;
    private String password;
    private String userName;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "game_id")
    private Game game;

    public Player() {
    }

    public Player(String userName, String password) {
        this.userName = userName;
        this.password = password;


    }

    //GETTERS & SETTERS//
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Score> getScores() {
        return scores;
    }

    public void setScores(Set<Score> scores) {
        this.scores = scores;
    }

    public Set<GamePlayer> getGameGamePlayer() {
        return gameGamePlayer;
    }

    public void setGameGamePlayer(Set<GamePlayer> gameGamePlayer) {
        this.gameGamePlayer = gameGamePlayer;
    }

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }
//    public GamePlayer getGamePlayers() {
//        return gamePlayers;
//    }
//
//    public void setGamePlayers(GamePlayer gamePlayers) {
//        this.gamePlayers = gamePlayers;
//    }

    //
//    @Autowired
//    private GamePlayer gamePlayers;
    //METHODS//
    public Map<String, Object> makeListDTO() {
        Map<String, Object> dto = new LinkedHashMap<>();
        dto.put("id", getId());
        dto.put("email", this.getUserName());
        return dto;
    }
//
//    public Map<String, Object> newPlayerDTO() {
//        Map<String, Object> dto = new LinkedHashMap<>();
//        dto.put("id",)
//
//
//    }


}











