package com.codeoftheweb.salvo;

import com.codeoftheweb.salvo.models.*;
import com.codeoftheweb.salvo.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.GlobalAuthenticationConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.Arrays;


@SpringBootApplication
public class SalvoApplication extends SpringBootServletInitializer {
    public static void main(String[] args) {
        SpringApplication.run(SalvoApplication.class, args);

    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return PasswordEncoderFactories.createDelegatingPasswordEncoder();
    }

    @Bean
    public CommandLineRunner initData(ScoresRepository scoresRepository,
                                      ShipRepository shipRepository,
                                      GameRepository gameRepository,
                                      PlayerRepository repository,
                                      GamePlayerRepository gamePlayerRepository,
                                      SalvoRepository salvoRepository) {
        return (args) -> {


            // PLAYERS //
            Player play = new Player("j.bauer@ctu.gov", passwordEncoder().encode("24"));
            Player play2 = new Player("c.obrian@ctu.gov", "42");
            Player play3 = new Player("kim_bauer@gmail.com", "kb");
            Player play4 = new Player("t.almeida@ctu.gov", "mole");

            // GAMES //
            Game game = new Game(LocalDateTime.now());
            Game game2 = new Game(LocalDateTime.now().plusHours(1));
            Game game3 = new Game(LocalDateTime.now().plusHours(2));
            Game game4 = new Game(LocalDateTime.now().plusHours(3));
            Game game5 = new Game(LocalDateTime.now().plusHours(4));
            Game game6 = new Game(LocalDateTime.now().plusHours(5));
            Game game7 = new Game(LocalDateTime.now().plusHours(6));


            //GAMEPLAYERS //
            GamePlayer gamePlayer = new GamePlayer(game, play);
            GamePlayer gamePlayer2 = new GamePlayer(game, play2);
            GamePlayer gamePlayer3 = new GamePlayer(game2, play);
            GamePlayer gamePlayer4 = new GamePlayer(game2, play2);
            GamePlayer gamePlayer5 = new GamePlayer(game3, play2);
            GamePlayer gamePlayer6 = new GamePlayer(game3, play4);
            GamePlayer gamePlayer7 = new GamePlayer(game4, play2);
            GamePlayer gamePlayer8 = new GamePlayer(game4, play);
            GamePlayer gamePlayer9 = new GamePlayer(game5, play4);
            GamePlayer gamePlayer10 = new GamePlayer(game5, play);

            //LOCATIONS//


            //SHIPS//
            Ship ship = new Ship("destroyer", Arrays.asList("H2", "H3", "H4"), gamePlayer);
            Ship ship1 = new Ship("submarine", Arrays.asList("E1", "F1", "G1"), gamePlayer);
            Ship ship2 = new Ship("patrol boat", Arrays.asList("B4", "B5"), gamePlayer);
            Ship ship3 = new Ship("destroyer", Arrays.asList("B5", "C5", "D5"), gamePlayer2);
            Ship ship4 = new Ship("patrol boat", Arrays.asList("F1", "F2"), gamePlayer2);
            Ship ship5 = new Ship("destroyer", Arrays.asList("B5", "C5", "D5"), gamePlayer3);
            Ship ship6 = new Ship("patrol boat", Arrays.asList("c6", "C7"), gamePlayer3);
            Ship ship7 = new Ship("submarine", Arrays.asList("A2", "A3", "A4"), gamePlayer4);
            Ship ship8 = new Ship("patrol boat", Arrays.asList("G6", "H6"), gamePlayer4);
            Ship ship9 = new Ship("destroyer", Arrays.asList("B5", "C5", "D5"), gamePlayer5);
            Ship ship10 = new Ship("patrol boat", Arrays.asList("C6", "C7"), gamePlayer5);
            Ship ship11 = new Ship("submarine", Arrays.asList("A2", "A3", "A4"), gamePlayer6);
            Ship ship12 = new Ship("patrol boat", Arrays.asList("G6", "H6"), gamePlayer6);
            Ship ship13 = new Ship("destroyer", Arrays.asList("B5", "C5", "D5"), gamePlayer7);
            Ship ship14 = new Ship("patrol boat", Arrays.asList("C6", "C7"), gamePlayer7);
            Ship ship15 = new Ship("submarine", Arrays.asList("A2", "A3", "A4"), gamePlayer8);
            Ship ship16 = new Ship("patrol boat", Arrays.asList("G6", "H6"), gamePlayer8);
            Ship ship17 = new Ship("destroyer", Arrays.asList("B5", "C5", "D5"), gamePlayer9);
            Ship ship18 = new Ship("patrol boat", Arrays.asList("C7", "C6"), gamePlayer9);
            Ship ship19 = new Ship("submarine", Arrays.asList("A2", "A3", "A4"), gamePlayer10);
            Ship ship20 = new Ship("patrol boat", Arrays.asList("G6", "H6"), gamePlayer10);


            // SALVOES//
            Salvo salvo = new Salvo(1, Arrays.asList("B5", "C5", "F1"), gamePlayer);
            Salvo salvo2 = new Salvo(1, Arrays.asList("B5", "B5", "B6"), gamePlayer2);
            Salvo salvo3 = new Salvo(2, Arrays.asList("F2", "D5"), gamePlayer);
            Salvo salvo4 = new Salvo(2, Arrays.asList("E1", "H3", "A2"), gamePlayer2);
            Salvo salvo5 = new Salvo(1, Arrays.asList("A2", "A4", "G6"), gamePlayer3);
            Salvo salvo6 = new Salvo(1, Arrays.asList("B5", "C5", "C7"), gamePlayer4);
            Salvo salvo7 = new Salvo(2, Arrays.asList("A3", "H6"), gamePlayer3);
            Salvo salvo8 = new Salvo(2, Arrays.asList("C5", "C6"), gamePlayer4);
            Salvo salvo9 = new Salvo(1, Arrays.asList("G6", "H6", "A4"), gamePlayer5);
            Salvo salvo10 = new Salvo(1, Arrays.asList("H1", "H2", "H3"), gamePlayer6);
            Salvo salvo11 = new Salvo(2, Arrays.asList("A2", "A3", "D8"), gamePlayer5);
            Salvo salvo12 = new Salvo(2, Arrays.asList("E1", "F2", "G3"), gamePlayer6);
            Salvo salvo13 = new Salvo(1, Arrays.asList("A3", "A4", "F7"), gamePlayer7);
            Salvo salvo14 = new Salvo(1, Arrays.asList("B5", "C6", "H1"), gamePlayer8);
            Salvo salvo15 = new Salvo(2, Arrays.asList("A2", "G6", "H6"), gamePlayer7);
            Salvo salvo16 = new Salvo(2, Arrays.asList("C5", "C7", "D5"), gamePlayer8);
            Salvo salvo17 = new Salvo(1, Arrays.asList("A1", "A2", "A3"), gamePlayer9);
            Salvo salvo18 = new Salvo(1, Arrays.asList("B5", "B6", "C7"), gamePlayer10);
            Salvo salvo19 = new Salvo(2, Arrays.asList("G6", "G7", "G8"), gamePlayer9);
            Salvo salvo20 = new Salvo(2, Arrays.asList("C6", "D6", "E6"), gamePlayer10);


            //SCORES//
            Score score = new Score(game, play, 1.0, LocalDateTime.now());
            Score score2 = new Score(game, play2, 0.0, LocalDateTime.now());
            Score score3 = new Score(game2, play, 0.5, LocalDateTime.now());
            Score score4 = new Score(game2, play2, 0.5, LocalDateTime.now());
            Score score5 = new Score(game3, play2, 1.0, LocalDateTime.now());
            Score score6 = new Score(game3, play4, 0.0, LocalDateTime.now());
            Score score7 = new Score(game4, play2, 0.5, LocalDateTime.now());
            Score score8 = new Score(game4, play, 0.5, LocalDateTime.now());
            Score score9 = new Score(game5, play4, 0.0, LocalDateTime.now());
            Score score10 = new Score(game5, play, 0.0, LocalDateTime.now());


            //PLAYER REPOSITORY//
            repository.save(play);
            repository.save(play2);
            repository.save(play3);
            repository.save(play4);
            //GAME REPOSITORY//
            gameRepository.save(game);
            gameRepository.save(game2);
            gameRepository.save(game3);
            gameRepository.save(game4);
            gameRepository.save(game5);
            gameRepository.save(game6);
            gameRepository.save(game7);
            //GAMEPLAYER REPOSITORY//
            gamePlayerRepository.save(gamePlayer);
            gamePlayerRepository.save(gamePlayer2);
            gamePlayerRepository.save(gamePlayer3);
            gamePlayerRepository.save(gamePlayer4);
            gamePlayerRepository.save(gamePlayer5);
            gamePlayerRepository.save(gamePlayer6);
            gamePlayerRepository.save(gamePlayer7);
            gamePlayerRepository.save(gamePlayer8);
            gamePlayerRepository.save(gamePlayer9);
            gamePlayerRepository.save(gamePlayer10);

            //SHIP REPOSITORY//
            shipRepository.save(ship);
            shipRepository.save(ship1);
            shipRepository.save(ship2);
            shipRepository.save(ship3);
            shipRepository.save(ship4);
            shipRepository.save(ship5);
            shipRepository.save(ship6);
            shipRepository.save(ship7);
            shipRepository.save(ship8);
            shipRepository.save(ship9);
            shipRepository.save(ship10);
            shipRepository.save(ship11);
            shipRepository.save(ship12);
            shipRepository.save(ship13);
            shipRepository.save(ship14);
            shipRepository.save(ship15);
            shipRepository.save(ship16);
            shipRepository.save(ship17);
            shipRepository.save(ship18);
            shipRepository.save(ship19);
            shipRepository.save(ship20);
            //SALVOES REPOSITORY//
            salvoRepository.save(salvo);
            salvoRepository.save(salvo2);
            salvoRepository.save(salvo3);
            salvoRepository.save(salvo4);
            salvoRepository.save(salvo5);
            salvoRepository.save(salvo6);
            salvoRepository.save(salvo7);
            salvoRepository.save(salvo8);
            salvoRepository.save(salvo9);
            salvoRepository.save(salvo10);
            salvoRepository.save(salvo11);
            salvoRepository.save(salvo12);
            salvoRepository.save(salvo13);
            salvoRepository.save(salvo14);
            salvoRepository.save(salvo15);
            salvoRepository.save(salvo16);
            salvoRepository.save(salvo17);
            salvoRepository.save(salvo18);
            salvoRepository.save(salvo19);
            salvoRepository.save(salvo20);

            //SCORES REPOSITORY//
            scoresRepository.save(score);
            scoresRepository.save(score2);
            scoresRepository.save(score3);
            scoresRepository.save(score4);
            scoresRepository.save(score5);
            scoresRepository.save(score6);
            scoresRepository.save(score7);
            scoresRepository.save(score8);
            scoresRepository.save(score9);
            scoresRepository.save(score10);


        };


    }
}
//USERNAME VALIDATION//

@Configuration
class WebSecurityConfiguration extends GlobalAuthenticationConfigurerAdapter {

    @Autowired
    PlayerRepository playerRepository;

    @Override
    public void init(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(inputUserName -> {
            Player player = playerRepository.findByUserName(inputUserName);
            if (player != null) {
                return new User(player.getUserName(), player.getPassword(), AuthorityUtils.createAuthorityList("USER"));
            } else {
                throw new UsernameNotFoundException("Unknown user:" + inputUserName);
            }
        });


    }
}

@EnableWebSecurity
@Configuration
class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/api/games/**").permitAll()
                .antMatchers("/api/players").permitAll()
                .antMatchers("/web/**").permitAll()
                .antMatchers("/h2-console/").permitAll()
                .antMatchers("/**").hasAuthority("USER")
                .antMatchers("/h2console").permitAll()
                .and();
        http.formLogin()
                .usernameParameter("name")
                .passwordParameter("pwd")
                .loginPage("/api/login");
        http.logout()
                .logoutUrl("/api/logout");

        // turn off checking for CSRF tokens
        http.csrf().disable();
        http.headers().frameOptions().disable();

        // if user is not authenticated, just send an authentication failure response
        http.exceptionHandling().authenticationEntryPoint((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

        // if login is successful, just clear the flags asking for authentication
        http.formLogin().successHandler((req, res, auth) -> clearAuthenticationAttributes(req));

        // if login fails, just send an authentication failure response
        http.formLogin().failureHandler((req, res, exc) -> res.sendError(HttpServletResponse.SC_UNAUTHORIZED));

        // if logout is successful, just send a success response
        http.logout().logoutSuccessHandler(new HttpStatusReturningLogoutSuccessHandler());
    }

    private void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        }

    }

}



